/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.dao;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import EjemploMySql.Pelicula;
import java.util.List;

/**
 *
 * @author Esteban
 */
public class PeliculaDao {

    private Connection cnx = null;

    public PeliculaDao() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException ex) {

            System.out.println("No se encontró el driver");

            Logger.getLogger(PeliculaDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            cnx = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/cine", "root", "111mil");
        } catch (SQLException ex) {

            System.out.println("no se encontró el motor de base de datos,la base de datos, eçl usuario o contraseña");

            Logger.getLogger(PeliculaDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void agregarPelicula() {

        try {
            Statement st = cnx.createStatement();

            st.executeUpdate(
                    "INSERT INTO `cine`.`pelicula` (`id_pelicula`, `estreno_pelicula`, `disponible_pelicula`, `duracion_pelicula`, `fecha_ingreso_pelicula`, `nombre_pelicula`, `titulo_original_pelicula`) "
                    + "VALUES ('5', '6/03/2015', 'falso', '125', '7/04/2015', 'Quinto Elemento', 'Quinto elemento');");

            // conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PeliculaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modificarPelicula() {

        try {
            Statement st = cnx.createStatement();

            st.executeUpdate("update pelicula set nombre_pelicula='Terminator' where cod_pelicula=2 ");

            // conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PeliculaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Pelicula> devolverPelicula() {

        ArrayList<Pelicula> listaPelis = new ArrayList<Pelicula>();

        try {
            Statement st;
            st = cnx.createStatement();
            ResultSet conjuntoResultados = st.executeQuery("select * from pelicula ");

            Pelicula peli;

            for (int i = 0; conjuntoResultados.next(); i++) {
                peli = new Pelicula();

                peli.setCod_pelicula(conjuntoResultados.getInt("id_pelicula"));
                peli.setDuracion_pelicula(conjuntoResultados.getInt("duracion_pelicula"));
                peli.setNombre_pelicula(conjuntoResultados.getString("nombre_pelicula"));

                listaPelis.add(peli);
            }

        } catch (SQLException ex) {

            Logger.getLogger(PeliculaDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaPelis;

    }

}
