/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjemploMySql;

import clases.dao.*;
import clases.dao.PeliculaDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Esteban
 */
public class EjemploMySql {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        ArrayList<Pelicula> listaPeliculas = new ArrayList<Pelicula>();

        PeliculaDao conexion = new PeliculaDao();
        //conexion.agregarPelicula();
        //conexion.modificarPelicula();
        listaPeliculas = conexion.devolverPelicula();

        for (int i = 0; i < listaPeliculas.size(); i++) {
            System.out.print(listaPeliculas.get(i).getCod_pelicula() + "---");
            System.out.print(listaPeliculas.get(i).getDuracion_pelicula() + "---");
            System.out.println(listaPeliculas.get(i).getNombre_pelicula());
        }

    }

}
