/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjemploMySql;

/**
 *
 * @author Esteban
 */
public class Pelicula {
    
    private int cod_pelicula;
    private String nombre_pelicula;
    private int duracion_pelicula;

    /**
     * @return the cod_pelicula
     */
    public int getCod_pelicula() {
        return cod_pelicula;
    }

    /**
     * @param cod_pelicula the cod_pelicula to set
     */
    public void setCod_pelicula(int cod_pelicula) {
        this.cod_pelicula = cod_pelicula;
    }

    /**
     * @return the nombre_pelicula
     */
    public String getNombre_pelicula() {
        return nombre_pelicula;
    }

    /**
     * @param nombre_pelicula the nombre_pelicula to set
     */
    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }

    /**
     * @return the duracion_pelicula
     */
    public int getDuracion_pelicula() {
        return duracion_pelicula;
    }

    /**
     * @param duracion_pelicula the duracion_pelicula to set
     */
    public void setDuracion_pelicula(int duracion_pelicula) {
        this.duracion_pelicula = duracion_pelicula;
    }
    
    
    
}
